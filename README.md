# Bees Java SDK

Bees provides the quickest way to integrate with the MPESA {daraja} SDKs. This repository contains the Java8+ wrapper API around Bees REST MPESA API.

## Getting Started

Please ensure that you have Java8+ installed to use these bindings.

### Examples


```
String key = "5ce0189ba....80560988c49";
String secret = "4185ec490c....88554c291641f";
Bees bees = new Bees(key, secret);

// get key and secret
System.out.println(String.format("Key: %s", bees.getKey()));
System.out.println(String.format("Secret: %s", bees.getSecret()));

// get user details
System.out.println(bees.getUserFactory().get("username"));

// create user
System.out.println(bees.getUserFactory().create("Theresa", "May", "theresa",
        "password", "0703133890", "theresa@gmail.com",
        new URL("https://bees-avatar-images.s3.amazonaws.com/1557909926.jpeg")));

// update user
System.out.println(bees.getUserFactory().update("3b3903f69af9ea78fadb88c2155866d4", "Theresay", "Mays",
        "theresaa", "password", "0703133891", "theresa@gmail.com",
        new URL("https://bees-avatar-images.s3.amazonaws.com/1557909926.jpeg")));

// get keywords
System.out.println(bees.getKeywordsFactory().getKeywords());

// create keywords
System.out.println(bees.getKeywordsFactory().createKeywords(new String[] {"ANOTHER"}, new URL("https://bees.software/log.php"), Env.live));

// delete keywords
System.out.println(bees.getKeywordsFactory().deleteKeyword("0c2b9e17a2d00cb6d23903005ce13bfe"));

// get banks
System.out.println(bees.getBanksFactory().getBanks());

// get account
System.out.println(bees.getAccountFactory().getAccount("007c3b67580ba36c8d300d8531a9bc14"));

// create account
System.out.println(bees.getAccountFactory().createAccount("3ec0192b733cd6d65bd9f5543dd41e5f", "AB12346785"));

// update account
System.out.println(bees.getAccountFactory().updateAccount("057a54733a74bc2310badd2e8cebd457", "3ec0192b733cd6d65bd9f5543dd41e5f", "0987655"));

// delete account
System.out.println(bees.getAccountFactory().deactivateAccount("057a54733a74bc2310badd2e8cebd457"));

// get credentials
System.out.println(bees.getCredentialsFactory().getCredentials());

// activate credentials
System.out.println(bees.getCredentialsFactory().activateCredentials("984c1e6735cc32583308ff3df675c28b"));

// deactivate credentials
System.out.println(bees.getCredentialsFactory().deactivateCredentials("984c1e6735cc32583308ff3df675c28b"));

// prompt stk push transaction
System.out.println(bees.getSTKPushTransactionFactory().promptSTKPushPayment(PaymentType.mpesa_stk_push,
        "254703133896", new URL("https://bees.software/log.php"), "Sweater",
        "Payment for sweater", 100, "057a54733a74bc2310badd2e8cebd457", Env.live));

// poll stk push transaction
System.out.println(bees.getSTKPushTransactionFactory().pollSTKPushTransaction(PaymentStatusType.mpesa_stk_poll,
        "771775f059d13610ca9e7f0b04b15c1b"));

// poll c2b transaction
System.out.println(bees.getC2BTransactionsFactory().pollTransactions(PaymentStatusType.mpesa_poll,
        "NEU998F3UJ", new URL("https://bees.software/log.php"), Env.live));

```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

