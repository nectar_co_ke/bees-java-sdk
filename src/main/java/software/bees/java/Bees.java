package software.bees.java;

import software.bees.java.models.*;

public class Bees {

    private String key;
    private String secret;

    public Bees(String key, String secret) {
        setKey(key);
        setSecret(secret);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String  getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public User getUserFactory() {
        return new User(key, secret);
    }

    public Account getAccountFactory() {
        return new Account(key, secret);
    }

    public C2BTransaction getC2BTransactionsFactory() {
        return new C2BTransaction(key, secret);
    }

    public STKPushTransaction getSTKPushTransactionFactory() {
        return new STKPushTransaction(key, secret);
    }

    public Credentials getCredentialsFactory() {
        return new Credentials(key, secret);
    }

    public Keywords getKeywordsFactory() {
        return new Keywords(key, secret);
    }

    public Banks getBanksFactory() {
        return new Banks(key, secret);
    }
}
