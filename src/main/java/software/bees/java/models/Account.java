package software.bees.java.models;

import software.bees.java.utils.Payload;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class Account extends Base {

    private final String ACCOUNT_PATH = "/v1/account";
    private final String CONTENT_TYPE= "application/json";

    public Account(String key, String secret) {
        super(key, secret);
    }

    public String createAccount(String bankRef, String accountNo)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createAccountParams(bankRef, accountNo));
        return super.post(ACCOUNT_PATH, payload, CONTENT_TYPE);
    }

    public String getAccount(String accountRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException  {
        return super.get(ACCOUNT_PATH, String.format("ref=%s", accountRef), CONTENT_TYPE);
    }

    public String updateAccount(String accountRef, String bankRef, String accountNo)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createUpdateAccountParams(accountRef, bankRef, accountNo));
        return super.put(ACCOUNT_PATH, payload, CONTENT_TYPE);
    }

    public String deactivateAccount(String accountRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException{
        return super.delete(ACCOUNT_PATH, String.format("account_ref=%s", accountRef), CONTENT_TYPE);
    }

    private HashMap<String, Object> createAccountParams(String bankRef, String accountNo) {
        HashMap<String, Object> accountParams = new HashMap<>();
        accountParams.put("bank_ref", bankRef);
        accountParams.put("account_no", accountNo);
        return accountParams;
    }

    private HashMap<String, Object> createUpdateAccountParams(String accountRef, String bankRef, String accountNo) {
        HashMap<String, Object> updateAccountParams = new HashMap<>();
        updateAccountParams.put("account_ref", accountRef);
        updateAccountParams.putAll(createAccountParams(bankRef, accountNo));
        return updateAccountParams;
    }
}
