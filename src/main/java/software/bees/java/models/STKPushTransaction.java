package software.bees.java.models;

import software.bees.java.env.Env;
import software.bees.java.paymentstatustypes.PaymentStatusType;
import software.bees.java.paymenttypes.PaymentType;
import software.bees.java.utils.Payload;

import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class STKPushTransaction extends Base {

    private final String PROMPT_STK_PAYMENTS_PATH = "/v1/promptSTKPushPayment";
    private final String POLL_STK_PAYMENTS_PATH = "/v1/stktransactions";

    public STKPushTransaction(String key, String secret) {
        super(key, secret);
    }

    public String promptSTKPushPayment(PaymentType paymentType, String payer,
                                     URL callbackUrl, String displayedDescription,
                                     String fullDescription, double amount,
                                     String accountRef, Env env)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createSTKPushParams(paymentType, payer, callbackUrl,
                                                            displayedDescription, fullDescription,
                                                            amount, accountRef, env));
        return super.post(PROMPT_STK_PAYMENTS_PATH, payload, CONTENT_TYPE);
    }

    public String pollSTKPushTransaction(PaymentStatusType paymentStatusType, String transactionRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.get(POLL_STK_PAYMENTS_PATH, createPollTransactionsRequestParams(paymentStatusType,
                                                    transactionRef), CONTENT_TYPE);
    }

    private HashMap<String, Object> createSTKPushParams(PaymentType paymentType, String payer,
                                                        URL callbackUrl, String displayedDescription,
                                                        String fullDescription, double amount,
                                                        String accountRef, Env env) {
        HashMap<String, Object> stkPushParams = new HashMap<>();
        stkPushParams.put("payment_type", paymentType.name());
        stkPushParams.put("payer", payer);
        stkPushParams.put("callback_url", callbackUrl.toString());
        stkPushParams.put("displayed_desc", displayedDescription);
        stkPushParams.put("full_desc", fullDescription);
        stkPushParams.put("amount", amount);
        stkPushParams.put("account_ref", accountRef);
        stkPushParams.put("env", env.name());
        return stkPushParams;
    }

    private String createPollTransactionsRequestParams(PaymentStatusType paymentStatusType, String transactionRef) {
        return String.format("payment_status_type=%s&ref=%s",
                paymentStatusType.name(), transactionRef);
    }
}
