package software.bees.java.models;

import software.bees.java.utils.Payload;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class Credentials extends Base {

    private final String CREDENTIALS_PATH = "/v1/credentials";

    public Credentials(String key, String secret) {
        super(key, secret);
    }

    public String createCredentials(String[] permissionIdentifiers)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createCredentialsParams(permissionIdentifiers));
        return super.post(CREDENTIALS_PATH, payload, CONTENT_TYPE);
    }

    public String getCredentials()
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.get(CREDENTIALS_PATH, "", CONTENT_TYPE);
    }

    public String activateCredentials(String credentialsRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        String path = String.format("%s/%s/activate", CREDENTIALS_PATH, credentialsRef);
        return super.put(path, null, CONTENT_TYPE);
    }

    public String deactivateCredentials(String credentialsRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        String path = String.format("%s/%s/deactivate", CREDENTIALS_PATH, credentialsRef);
        return super.put(path, null, CONTENT_TYPE);
    }

    private HashMap<String, Object> createCredentialsParams(String[] permissions) {
        HashMap<String, Object> params = new HashMap<>();
        for(String permission : permissions) {
            params.put("permission", permission);
        }
        return params;
    }
}
