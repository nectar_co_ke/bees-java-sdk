package software.bees.java.models;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Banks extends Base {

    private final String BANKS_PATH = "/v1/banks";

    public Banks(String key, String secret) {
            super(key, secret);
    }

    public String getBanks()
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.get(BANKS_PATH, "", CONTENT_TYPE);
    }

}
