package software.bees.java.models;

import software.bees.java.env.Env;
import software.bees.java.paymentstatustypes.PaymentStatusType;

import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class C2BTransaction extends Base {

    private final String POLL_C2B_PAYMENTS_PATH = "/v1/transactions";

    public C2BTransaction(String key, String secret) {
        super(key, secret);
    }

    public String pollTransactions(PaymentStatusType paymentStatusType, String transactionID,
                                 URL callbackUrl, Env env)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.get(POLL_C2B_PAYMENTS_PATH, createPollTransactionsRequestParams(paymentStatusType,
                transactionID, callbackUrl, env), CONTENT_TYPE);
    }

    private String createPollTransactionsRequestParams(PaymentStatusType paymentStatusType, String transactionID,
                                                       URL callbackUrl, Env env) {
        return String.format("payment_status_type=%s&transaction_id=%s&callback_url=%s&env=%s",
                paymentStatusType.name(), transactionID, callbackUrl.toString(), env.name());
    }
}
