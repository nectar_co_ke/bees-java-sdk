package software.bees.java.models;

import software.bees.java.utils.Payload;

import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Optional;

public class User extends Base {

    private final String USER_PATH = "/v1/user";

    public User(String key, String secret) {
        super(key, secret);
    }

    public Optional<String> create(String firstName, String lastName, String username,
                                   String password, String phoneNo, String email, URL imageUrl)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createUserParams(firstName, lastName, username,
                                        password, phoneNo, email, imageUrl));
        String ref = super.post(USER_PATH, payload, CONTENT_TYPE);
        return Optional.of(ref);
    }

    public String get(String username)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        String path = String.format("username=%s", username);
        return super.get(USER_PATH, path, CONTENT_TYPE);
    }

    public String update(String ref, String firstName, String lastName, String username,
                         String password, String phoneNo, String email, URL imageUrl)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        Payload payload = createPayload(createUpdateUserParams(ref, firstName, lastName, username,
                                                               password, phoneNo, email, imageUrl));
        return super.put(USER_PATH, payload, CONTENT_TYPE);
    }

    public String delete(String userRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.delete(USER_PATH, String.format("?%s", userRef), CONTENT_TYPE);
    }


    private HashMap<String, Object> createUserParams(String firstName, String lastName, String username,
                                                     String password, String phoneNo, String email,
                                                     URL imageUrl) {
        HashMap<String, Object> userParams = new HashMap<>();
        userParams.put("first_name", firstName);
        userParams.put("last_name", lastName);
        userParams.put("username", username);
        userParams.put("password", password);
        userParams.put("phone_no", phoneNo);
        userParams.put("email", email);
        userParams.put("image_url", imageUrl.toString());
        return userParams;
    }

    private HashMap<String, Object> createUpdateUserParams(String ref, String firstName,
                                                           String lastName, String username,
                                                           String password, String phoneNo,
                                                           String email, URL imageUrl) {
        HashMap<String, Object> createUserParams = new HashMap<>();
        HashMap<String, Object> userParams = createUserParams(firstName, lastName, username,
                                                                password, phoneNo, email, imageUrl);
        createUserParams.put("ref", ref);
        createUserParams.putAll(userParams);
       return createUserParams;
    }
}
