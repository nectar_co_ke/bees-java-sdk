package software.bees.java.models;

import software.bees.java.env.Env;
import software.bees.java.utils.Payload;

import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Keywords extends Base {

    private final String KEYWORDS_PATH = "/v1/keywords";

    public Keywords(String key, String secret) {
        super(key, secret);
    }

    public String getKeywords()
            throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        return super.get(KEYWORDS_PATH, "", CONTENT_TYPE);
    }

    public String createKeywords(String[] keywords, URL confirmationUrl, Env env)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException{
        Payload payload = createPayload(createKeywordsParams(keywords, confirmationUrl, env));
        return super.post(KEYWORDS_PATH, payload, CONTENT_TYPE);
    }

    public String deleteKeyword(String keywordRef)
            throws NoSuchAlgorithmException, InvalidKeyException, IOException{
        String endpoint = String.format("%s/%s", KEYWORDS_PATH, keywordRef);
        return super.delete(endpoint, "", CONTENT_TYPE);
    }

    private HashMap<String, Object> createKeywordsParams(String[] keywords, URL confirmationUrl, Env env) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("keywords", Arrays.asList(keywords).stream().collect(Collectors.joining(",")));
        params.put("confirmation_url", confirmationUrl.toString());
        params.put("env", env.name());
        return params;
    }
}
