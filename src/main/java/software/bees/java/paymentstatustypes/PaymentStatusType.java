package software.bees.java.paymentstatustypes;

public enum PaymentStatusType {
    mpesa_stk_poll, mpesa_poll
}
